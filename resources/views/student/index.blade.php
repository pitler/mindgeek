@extends('master')

@section('content')

<div class= "row">
	<div class = "col-md-12">
		<br />
		<h3 align= "center">Student data</h3>
		<br />
		<table class = "table table-bordered">
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>Action</th>
			</tr>
			@foreach($students as $row)
				<tr>
					<td>{{$row['first_name']}}</td>
					<td>{{$row['last_name']}}</td>					
					<td>
						<a href="{{ route('getfile', ['userId' => $row['id']]) }}" class=" btn btn-primary">
    				Generate results
					</a>
					</td>
				</tr>
			@endforeach

		</table>

	</div>
</div>


@endsection