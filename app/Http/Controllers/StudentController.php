<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\SchoolBoard;

use Illuminate\Support\Facades\DB;

use View;

use File;

use Response;



class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //Take the info from the database

        $students = Student::all()->toArray();
        return view("student.index", compact('students') );
    }

  
     /**
     * In this function generate the files depending on the school board 
     *
     * @param  int  $userId     The id of the student     
     */
    public function generate($userId)
    {

        //Take the info for the student
        $results = DB::select('select st.id, st.first_name, st.last_name, st.grades, st.sboard, sb.json from students st, school_boards sb 
            where st.id = :id
            and sb.id = st.sboard', ['id' => $userId]);

        //Prepare the results
        if($results)
        {
            $resultArray = array();
            $results = $results[0];
            $gradesArray = explode(",", $results->grades);
            $name = $results->first_name." " . $results->last_name;
            $board = $results->sboard;
            $json = $results->json;

            //Take the average of the grades
            $average = $this->getAverage($board, $gradesArray);
            $avResult = "Fail";

            //If the average is > 7 it Pass, if not it fails
            if($average >= 7)
            {
                $avResult = "Pass";
            }

            //Put the results into an array
            $resultArray =['StudentId' => $userId, 'Name' => $name, 'Grades' => $gradesArray, 'Average' => $average, 'Final' => $avResult];

            //If school boear is CMS, generate the json file
            if($board == 1)
            {
                $resultArray =json_encode(['StudentId' => $userId, 'Name' => $name, 'Grades' => $gradesArray, 'Average' => $average, 'Final' => $avResult]);

                $this->setStudenDataJson($name, $resultArray);    
            }
            //If school board is CMSB, generate xml file
            else
            {
                $this->setStudenDataXml($name, $resultArray);    
            }
        }
        //If there an error, send the basic error view
        else
        {
            return view("student.error");
        }

        //Return the basic view for success
         return view("student.result");
    }


     /**
     * Generate the file in xml format
     *
     * @param  String  $name            The name of the student
     * @param  Array   $resultArray     The array wit the results to create the file
     * 
     */
    private function setStudenDataXml($name, $resultArray)
    {

       //Create the xml document
        $dom = new \DOMDocument();
        $dom->encoding = 'utf-8';
        $dom->xmlVersion = '1.0';
        $dom->formatOutput = true;        
        $root = $dom->createElement('student');

        $studentName = $dom->createElement('name', $name);
        $studentId = $dom->createElement('id', $resultArray['StudentId']);
        $studentAverage = $dom->createElement('average', $resultArray['Average']);
        $studentFinal = $dom->createElement('final', $resultArray['Final']);

        $grades = $dom->createElement('grades');

        foreach ($resultArray['Grades'] as $value) 
        {
            $grade = $dom->createElement('grade', $value);
            $grades->appendChild($grade);
        }

        $root->appendChild($studentId);
        $root->appendChild($studentName);
        $root->appendChild($studentAverage);
        $root->appendChild($studentFinal);
        $root->appendChild($grades);

        $dom->appendChild($root);

        $xmlData = $dom->saveXML();

        //Validate if the repository  exists, if not, we create it
        if(!is_dir(public_path('/files/')))
        {
            mkdir(public_path('/files'), 0777, true);
        }   

        //Save the file in xml format
        File::put(public_path('/files/'.$name."_".time().".xml"),$xmlData);

    }


    /**
     * Generate the file in json format
     *
     * @param  String  $name            The name of the student
     * @param  Array   $resultArray     The array wit the results to create the file
     * 
     */
    private function setStudenDataJson($name, $resultArray)
    {

             //Validate if the repository  exists, if not, we create it
            if(!is_dir(public_path('/files/')))
            {
                    mkdir(public_path('/files'), 0777, true);
            }    

            //Save the file in xml format
            File::put(public_path('/files/'.$name.time().".json"),$resultArray);

    }


    /**
    *   Get the average of the grades for the student
    *   @param int      $board          The type of school board
    *   @param Array    $gradesArray    An array with the grades for the student
    **/
    private function getAverage($board, $gradesArray)
    {    

        
        $result = 0;
        //Verify the school board
        switch ($board) {
            case 1:

                $size = sizeof($gradesArray);
                $total = 0;

                //Read all the grades and take the average
                foreach ($gradesArray as $value) 
                {
                    $total += $value;
                }

                $result = round($total/$size, 2);
                
                break;

            case 2: 
            
                $size = sizeof($gradesArray);

                //Validate if there are more than 2 grades
                //If so, we disscard the lowest grade (just one)
                if($size > 2)
                {
                    $min = min($gradesArray);
                    $minKey=array_keys($gradesArray, $min);                        
                    unset($gradesArray[$minKey[0]]);                                                        
                }

                //Get the new size of the array
                $size = sizeof($gradesArray);

                $total = 0;

                 //Read all the grades and take the average
                foreach ($gradesArray as $value) 
                {
                    $total += $value;
                }

                $result = round($total/$size, 2);

            break;
            default:
                
                break;
        }

        return $result;
    }   

}